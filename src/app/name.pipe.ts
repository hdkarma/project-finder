import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'namePipe', pure: false})
export class NamePipe implements PipeTransform {
  transform(value: string): string {
    let purename = value.split(" ")
    var ret = "";
    purename.map(item=>{
      if(item[0]!="undefined")
        ret+= item[0].toUpperCase()
    })
    return ret;
  }
}