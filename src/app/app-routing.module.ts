import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsHomeComponent } from './pages/projects-home/projects-home.component';
import { Page404Component } from './pages/page404/page404.component';

// Routing module to access home page.
const routes: Routes = [
  {
    path: '',
    component: ProjectsHomeComponent,
  },
  
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
