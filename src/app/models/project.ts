// holds all projects and its count
export class Project {
    items: ProjectItem[];
    total_count: number;
  }
  
  // holds project details "name, address, designer, client, contractor, architect, status, start_date, end_date"
  export interface ProjectItem {
    name: string;
    address: string;
    designer: string;
    client: string;
    contractor: string;
    architect: string;
    status: string;
    start_date: Date;
    end_date: Date;
  }
  
// Helper function for initialisation of Project Array. This returns a default project array.
export const initializeState = (): Project[] => {
  return Array<Project>({ items: [], total_count: 0 });
};