import { Project } from '../models/project';
import { createReducer, on, Action } from '@ngrx/store';
import * as ProjectsActions from './projects.actions';
import { initializeState } from '../models/project';
export const intialState = initializeState();

// Reducer which change the state of element.
const reducer = createReducer(
  intialState,
  on(ProjectsActions.projectsUpdated, state => state),
);

// Reducer function to change the state value. This function accepts two parameters.
export function ProjectReducer(state: Project[] | undefined, action: ProjectsActions.ProjectAdd) {
  switch(action.type){
    case "ADD" : return reducer(action.projects, action);
    default : return reducer(action.projects, action);
  }
}