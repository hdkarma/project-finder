import {Action} from '@ngrx/store';
import { createAction, props } from '@ngrx/store';
import { ProjectItem } from '../models/project';

// Declaring action for projects to update
export const projectsUpdated = createAction(
  '[Projects] Projects Updated',
  props<{ projects: ProjectItem[] }>()
);

// Action for adding project to store the project. 
// Action name is "ProjectAdd"
export class ProjectAdd implements Action {
  readonly type = "ADD";
  constructor(public projects: any) {
  }
}