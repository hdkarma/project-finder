import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from './models/project';


@Injectable()
export class DataService {
    constructor(private _httpClient: HttpClient) {}
    getAllProjects(): Observable<Project> {
      /* defining link to the json api file. '/assets/project-data.json' Json file holds three keys 
      *items - Holds all project data. Items holds values "contractor, address, client, name, architect, designer, status, start_date, end_date"
      *total_count - holds total count of objects in items.
       */
      const requestUrl = window.location.origin+'/assets/project-data.json';
      // return data inside the json file 'project-data.json'.
      return this._httpClient.get<Project>(requestUrl);
    }
}
