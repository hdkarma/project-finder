import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../../data.service'
import {merge, Observable, of as observableOf} from 'rxjs';
import { MatSort, MatPaginator } from '@angular/material';
import {catchError, map, startWith, switchMap} from 'rxjs/operators'
import {Project, ProjectItem} from '../../models/project'

import {animate, state, style, transition, trigger} from '@angular/animations';
import {select, Store} from '@ngrx/store'; 
import * as ProjectActions from 'src/app/store/projects.actions';
import { DatePipe } from '@angular/common'
import { NamePipe } from 'src/app/name.pipe';
@Component({
  selector: 'app-projects-home',
  templateUrl: './projects-home.component.html',
  styleUrls: ['./projects-home.component.scss'],
  providers: [NamePipe],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),]
})
export class ProjectsHomeComponent implements AfterViewInit {

  // Variable for observable data.
  dataSource:Observable<Project[]>;
  resultsLength = 0;
  // Fields to display on table.
  displayedColumns: string[] = ['name', 'client', 'contractor', 'status', 'start_date', 'delete'];
  
  // Variable for storing data.
  data: ProjectItem[] = [];

  // to set the expanded element
  expandedElement = 'collapse'
  
  // Viewchild to access paginator and matsort.
  @ViewChild(MatPaginator, {static : false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static : false}) sort: MatSort;

  // service instance declaration and store. 
  constructor(private service: DataService,private store: Store<{ projects: Project[] }>) {
    this.dataSource = this.store.pipe(select('projects'));
    // For demonstrating the ngRx state update. This will log the state changes into console
    this.dataSource.subscribe(data=>console.log(data))
  }

  // For running sort operation
  public compare(a, b, order) {
    //converting to uppercase to avoid problem with lowecase names.
    a = a.toUpperCase()
    b = b.toUpperCase()
    if(order=="asc")
    // if ascending order 
      return (a > b) ? 1 : ((b > a) ? -1 : 0)
    else
      // if descending order 
      return (a < b) ? 1 : ((b < a) ? -1 : 0)
  }

  public delete(row){
    this.data = this.data.filter(function( obj ) {
      return obj != row;
    });
  }


  ngAfterViewInit() {

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // creating pipe to detect the sortchages in table.
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          //fetching all projects using function getAllProjects inside service class
          return this.service.getAllProjects();
        }),
        map(data => {
          // setting total count to the object received.
          this.resultsLength = data.total_count;
          // For running sort operation. 
          // OPTIMIZATION : Instead of calling the API again calling it once and storing the data inside the state to avoid multiple calls.
          data.items.sort(
            (a,b) => {
              // For comparing the list values using helper function.
              return this.compare(a[this.sort.active], b[this.sort.active],this.sort.direction )
            }
          )
          data.items.map((item, index)=>{
            // data.items[index].start_date = this.datepipe.transform(data.items[index].start_date, 'dd-MM-yyyy');

            // const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' })

          })
          // Current item set to index * 10
          let currentItem = this.paginator.pageIndex*10;
          // For pagination - calculating current index and next 10 items.
          // OPTIMIZATION : Filter and sort operation done on front end side to improve performance.
          return data.items.filter((e, index)=> index>=(currentItem) && index<=(currentItem+9));
        }),
        catchError((e) => {
          console.log(e)
        // Return null value if any error.
          return observableOf([]);
        })
      ).subscribe(data => {
        // setting variable data for the table to display
        this.data = data; 
        // action to dispatch the changes to the store - ngRx
        this.store.dispatch(new ProjectActions.ProjectAdd(data))
      });
  }
}




