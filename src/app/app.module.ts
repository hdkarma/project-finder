import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DataService } from './data.service';
import { HttpModule } from '@angular/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsHomeComponent } from './pages/projects-home/projects-home.component';
import { Page404Component } from './pages/page404/page404.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatExpansionModule} from '@angular/material';
import { StoreModule } from '@ngrx/store'; 
import { ProjectReducer } from './store/project.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NamePipe } from './name.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ProjectsHomeComponent,
    Page404Component,
    NamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    StoreModule.forRoot({ projects: ProjectReducer }) ,// ngRx store intiialize
    StoreDevtoolsModule.instrument(
      {maxAge: 10}
    ), // debugger for ngRx
  ],
  providers: [HttpClientModule, DataService, NamePipe], // Exporting service to make it available.
  bootstrap: [AppComponent]
})
export class AppModule { }

